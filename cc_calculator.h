//
// Created by vit on 2/3/21.
//

#ifndef CONSOLECALCULATOR_CC_CALCULATOR_H
#define CONSOLECALCULATOR_CC_CALCULATOR_H

#include <stdexcept>
#include <algorithm>
#include <cctype>
#include <stack>
#include <string>
#include <vector>

#include "gtest/gtest.h"


class CcCalculator
{
public:

    // Парсит строку на элементы: операнды, операторы, скобки.
    // Удаляет пробелы, проверяет на недопустимые символы, правильный формат чисел и положение скобок
    // При возникновении ошибки завершается, тогда *success == false,
    // а в возвращаемом значении ошибочный символ или число
    static std::vector<std::string> parse (std::string & expr, bool *success);

    // Производит вычисление выражения, неявно переводя его в обратную польскую запись
    // При неудае возвращает 0.0, *success == false
    static double calculate (const std::vector<std::string> & expr, bool *success);

private:

    static bool isOperator (const char & ch)
    {
        return ch == '*' || ch == '/' || ch == '+' || ch == '-';
    }

    // Допускаются унарными только плюс и минус
    static bool mayBeUnary (const char & ch)
    {
        return ch == '+' || ch == '-';
    }

    static bool isBracket (const char & ch)
    {
        return ch == '(' || ch == ')';
    }

    // Допустимы два десятичных разделителя
    static bool isDecimalDelimiter (const char & ch)
    {
        return ch == '.' || ch == ',';
    }

    // Может ли входить символ в состав числа с плавающей точкой
    static bool isPartOfNumber (const char & ch)
    {
        return isDecimalDelimiter(ch) || std::isdigit(static_cast<unsigned char>(ch));
    }

    // Является ли символ одним из разрешённых
    static bool isAllowed (const char & ch)
    {
        return isOperator(ch) || isBracket(ch) || isPartOfNumber(ch);
    }

    FRIEND_TEST(CcDoubleTest, IsDoubleTest);
    // Является ли строка числом с плавающей точкой
    // (может ли она целиком быть корректно сконвертирована в double)
    static bool isDouble (const std::string & s, double *value = nullptr);

    FRIEND_TEST(CcDoubleTest, OperatorsPriorityTest);
    // Возвращает приоритет оператора op, приянтый в калькуляторе. Если op не оператор, вернёт -1
    static int operatorPriority (const char & op);

    FRIEND_TEST(CcDoubleTest, OperationsTest);
    // Вычисляет операцию, диктуемую operator_, над числами (или числом, если оператор унарный)
    // на вершине стека operands. Операнды удаляются из стека.
    // Если в стеке недостаточно операндов или передан недопустимый оператор,
    // вернёт false, оставив стек в исходном состоянии (вернёт в него операнды).
    // Иначе результат вычисления положит в стек и вернёт true.
    static bool calcOperation (std::stack<double> &operands, const char & operator_);

};


#endif //CONSOLECALCULATOR_CC_CALCULATOR_H
