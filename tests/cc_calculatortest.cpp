//
// Created by vit on 2/8/21.
//

#include "../cc_calculator.cpp"

TEST(CcDoubleTest, IsDoubleTest)
{
    EXPECT_TRUE (CcCalculator::isDouble(".2"));
    EXPECT_TRUE (CcCalculator::isDouble("-0.2"));
    EXPECT_TRUE (CcCalculator::isDouble("-2."));
    EXPECT_TRUE (CcCalculator::isDouble("5"));
    EXPECT_TRUE (CcCalculator::isDouble("+5"));

    EXPECT_FALSE (CcCalculator::isDouble(""));
    EXPECT_FALSE (CcCalculator::isDouble("akde"));
    EXPECT_FALSE (CcCalculator::isDouble("ak^de&"));
    EXPECT_FALSE (CcCalculator::isDouble("5.2.1"));
    EXPECT_FALSE (CcCalculator::isDouble("5..1"));
    EXPECT_FALSE (CcCalculator::isDouble("..1"));
    EXPECT_FALSE (CcCalculator::isDouble("2.."));
}


TEST(CcDoubleTest,OperatorsPriorityTest)
{
    EXPECT_TRUE(CcCalculator::operatorPriority('/') > CcCalculator::operatorPriority('*'));
    EXPECT_TRUE(CcCalculator::operatorPriority('*') > CcCalculator::operatorPriority('+'));
    EXPECT_TRUE(CcCalculator::operatorPriority('-') > CcCalculator::operatorPriority('+'));
    EXPECT_TRUE(CcCalculator::operatorPriority(-'-') > CcCalculator::operatorPriority('/'));
    EXPECT_TRUE(CcCalculator::operatorPriority(-'+') > CcCalculator::operatorPriority('/'));
    EXPECT_TRUE(CcCalculator::operatorPriority('+') > CcCalculator::operatorPriority('('));
    EXPECT_TRUE(CcCalculator::operatorPriority('(') == CcCalculator::operatorPriority(')'));

    EXPECT_TRUE(CcCalculator::operatorPriority('+') > CcCalculator::operatorPriority('a'));
    EXPECT_TRUE(CcCalculator::operatorPriority('+') > CcCalculator::operatorPriority(-'/'));
    EXPECT_TRUE(CcCalculator::operatorPriority('+') > CcCalculator::operatorPriority('^'));
    EXPECT_TRUE(CcCalculator::operatorPriority('+') > CcCalculator::operatorPriority('1'));
}


TEST(CcDoubleTest, OperationsTest)
{
    std::stack<double> s;
    s.push(3);
    s.push(4.1);
    EXPECT_TRUE(CcCalculator::calcOperation(s, -'+'));
    EXPECT_TRUE(CcCalculator::calcOperation(s, '*'));
    EXPECT_FALSE(CcCalculator::calcOperation(s, '/'));
    EXPECT_TRUE(s.size() == 1);

    s.push(4.1);

    EXPECT_FALSE(CcCalculator::calcOperation(s, '2'));
    EXPECT_TRUE(s.size() == 2);

    EXPECT_TRUE(CcCalculator::calcOperation(s, '+'));
    EXPECT_TRUE(CcCalculator::calcOperation(s, -'-'));
    EXPECT_EQ(s.size(), 1);
    EXPECT_DOUBLE_EQ(s.top(), -16.4);
}


class CcExpressionTest : public ::testing::Test
{
protected:

    // Отрицательные кейсы по синтаксису
    std::string neg0 = "1.1 + 2.1 + abc";
    std::string neg01 = "";
    std::string neg02 = ".1 + 2.1 + 2^2";
    std::string neg1 = "1.5 + (2 -) 3 ";
    std::string neg11 = "1.5 (2 - 3) ";

    std::string neg12 = "1.5 + (2 - 3) * ";
    std::string neg2 = "2,1 + 1)";
    std::string neg3 = "2 *(4 + 3,1";
    std::string neg4 = "12,3,1 -2";
    std::string neg5 = "12..3 -2";
    std::string neg6 = ") + 5 * 3 ";
    std::string neg7 = " + 5 * 3 + (";

    // Отрицательные кейсы по вычислению
    // Деление на 0
    std::string neg_a0 = "1/0";
    std::string neg_a1 = "(1.2  + 5.1*0,3 ) / (6.2 -2*3,1)";


    // Положительные кейсы
    std::string pos00 = "-1 + 5 - 3";
    double pos00_val = 1;
    std::string pos01 = "-10 + (8 * 2.5) - (3 / 1,5)";
    double pos01_val = 8;
    std::string pos02 = "1 + (2 * (2.5 + 2.5 + (3 - 2))) - (3 / 1.5)";
    double pos02_val = 11;
    std::string pos0 = "1/2 + 3*(-4)";
    double pos0_val = -11.5;
    std::string pos1 = "(+2,2 + 4*3) /( 1-2 )";
    double pos1_val = -14.2;
    std::string pos2 = "1+ ( -2 * 3) -12.5";
    double pos2_val = -17.5;
    std::string pos3 = "- 4.5 +(8 - 2* 3.5) *0,1";
    double pos3_val = -4.4;
    std::string pos4 = "1- (0.5 + 1) / (-3,2)";
    double pos4_val = 1.46875;
    std::string pos5 = "(1 +2 * 1.2) / (2,7 -4.1) -3";
    double pos5_val = -5.42857142857142857143;
    std::string pos6 = "+1 + (4 + 2/2 * 12/ (4+1.2) -5,1) /3,3";
    double pos6_val = 1.36596736596736596737;
    std::string pos7 = "2. + 6";
    double pos7_val = 8;
    std::string pos8 = ".6 +4";
    double pos8_val = 4.6;
};

TEST_F(CcExpressionTest, ParsingNegativeTest)
{
    bool ok;
    std::vector<std::string> v1, v2;


    v1 = CcCalculator::parse(neg0, &ok);
    v2 = {"a"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_FALSE (ok);

    v1 = CcCalculator::parse(neg01, &ok);
    v2 = {""};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_FALSE (ok);

    v1 = CcCalculator::parse(neg02, &ok);
    v2 = {"^"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_FALSE (ok);

    v1 = CcCalculator::parse(neg1, &ok);
    v2 = {")"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_FALSE (ok);

    v1 = CcCalculator::parse(neg11, &ok);
    v2 = {"("};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_FALSE (ok);

    v1 = CcCalculator::parse(neg4, &ok);
    v2 = {"12.3.1"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_FALSE (ok);

    v1 = CcCalculator::parse(neg5, &ok);
    v2 = {"12..3"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_FALSE (ok);

}

TEST_F(CcExpressionTest, ParsingPositiveTest)
{
    bool ok;
    std::vector<std::string> v1, v2;


    v1 = CcCalculator::parse(neg12, &ok);
    v2 = {"1.5", "+", "(", "2", "-", "3", ")", "*"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);

    v1 = CcCalculator::parse(neg2, &ok);
    v2 = {"2.1", "+", "1", ")"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);

    v1 = CcCalculator::parse(neg3, &ok);
    v2 = {"2", "*", "(", "4", "+", "3.1"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);

    v1 = CcCalculator::parse(neg6, &ok);
    v2 = {")", "+", "5", "*", "3"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);

    v1 = CcCalculator::parse(neg7, &ok);
    v2 = {"+", "5", "*", "3", "+", "("};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);

    v1 = CcCalculator::parse(neg_a0, &ok);
    v2 = {"1", "/", "0"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);

    v1 = CcCalculator::parse(neg_a1, &ok);
    v2 = {"(", "1.2", "+", "5.1", "*", "0.3", ")", "/", "(", "6.2", "-", "2", "*", "3.1", ")"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);


    v1 = CcCalculator::parse(pos0, &ok);
    v2 = {"1", "/", "2", "+", "3", "*", "(", "-", "4", ")"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);

    v1 = CcCalculator::parse(pos1, &ok);
    v2 = {"(", "+", "2.2", "+", "4", "*", "3", ")", "/", "(", "1", "-", "2", ")"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);

    v1 = CcCalculator::parse(pos2, &ok);
    v2 = {"1", "+", "(", "-", "2", "*", "3", ")", "-", "12.5"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);

    v1 = CcCalculator::parse(pos3, &ok);
    v2 = {"-", "4.5", "+", "(", "8", "-", "2", "*", "3.5", ")", "*", "0.1"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);

    v1 = CcCalculator::parse(pos4, &ok);
    v2 = {"1", "-", "(", "0.5", "+", "1", ")", "/", "(", "-", "3.2", ")"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);

    v1 = CcCalculator::parse(pos5, &ok);
    v2 = {"(", "1", "+", "2", "*", "1.2", ")", "/", "(", "2.7", "-", "4.1", ")", "-", "3"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);

    v1 = CcCalculator::parse(pos6, &ok);
    v2 = {"+", "1", "+", "(", "4", "+", "2", "/", "2", "*", "12", "/", "(", "4", "+", "1.2", ")", "-", "5.1", ")", "/", "3.3"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);

    v1 = CcCalculator::parse(pos7, &ok);
    v2 = {"2.", "+", "6"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);

    v1 = CcCalculator::parse(pos8, &ok);
    v2 = {".6", "+", "4"};
    EXPECT_TRUE (std::equal(v1.begin(), v1.end(), v2.begin()));
    EXPECT_TRUE (ok);
}


TEST_F(CcExpressionTest, CalculationNegativeTest)
{
    bool pok, cok;


    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(neg12, &pok), &cok), .0);
    EXPECT_TRUE (pok);
    EXPECT_FALSE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(neg2, &pok), &cok), .0);
    EXPECT_TRUE (pok);
    EXPECT_FALSE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(neg3, &pok), &cok), .0);
    EXPECT_TRUE (pok);
    EXPECT_FALSE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(neg6, &pok), &cok), .0);
    EXPECT_TRUE (pok);
    EXPECT_FALSE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(neg7, &pok), &cok), .0);
    EXPECT_TRUE (pok);
    EXPECT_FALSE (cok);

}


TEST_F(CcExpressionTest, CalculationPositiveTest)
{
    bool pok, cok;

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(pos0, &pok), &cok), pos0_val);
    EXPECT_TRUE (pok);
    EXPECT_TRUE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(pos00, &pok), &cok), pos00_val);
    EXPECT_TRUE (pok);
    EXPECT_TRUE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(pos01, &pok), &cok), pos01_val);
    EXPECT_TRUE (pok);
    EXPECT_TRUE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(pos02, &pok), &cok), pos02_val);
    EXPECT_TRUE (pok);
    EXPECT_TRUE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(pos1, &pok), &cok), pos1_val);
    EXPECT_TRUE (pok);
    EXPECT_TRUE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(pos2, &pok), &cok), pos2_val);
    EXPECT_TRUE (pok);
    EXPECT_TRUE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(pos3, &pok), &cok), pos3_val);
    EXPECT_TRUE (pok);
    EXPECT_TRUE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(pos4, &pok), &cok), pos4_val);
    EXPECT_TRUE (pok);
    EXPECT_TRUE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(pos5, &pok), &cok), pos5_val);
    EXPECT_TRUE (pok);
    EXPECT_TRUE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(pos6, &pok), &cok), pos6_val);
    EXPECT_TRUE (pok);
    EXPECT_TRUE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(pos7, &pok), &cok), pos7_val);
    EXPECT_TRUE (pok);
    EXPECT_TRUE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(pos8, &pok), &cok), pos8_val);
    EXPECT_TRUE (pok);
    EXPECT_TRUE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(neg_a0, &pok), &cok), std::numeric_limits<double>::infinity());
    EXPECT_TRUE (pok);
    EXPECT_TRUE (cok);

    EXPECT_DOUBLE_EQ (CcCalculator::calculate(CcCalculator::parse(neg_a1, &pok), &cok), std::numeric_limits<double>::infinity());
    EXPECT_TRUE (pok);
    EXPECT_TRUE (cok);
}

 
