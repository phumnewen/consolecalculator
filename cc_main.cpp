
#include "cc_calculator.h"
#include <iostream>

void usageMessage ()
{
    std::cout  << "Использование:" << std::endl;
    std::cout  << "    ./consoleCalculator <expression>     вычислить выражение.\n"
                  "                                         Если expression cодержит скобки,\n"
                  "                                         оно должно заключаться в кавычки." << std::endl;
    std::cout  << "\n    ./consoleCalculator -h               Вывести эту справку\n" << std::endl;
}

int main(int argc, char ** argv)
{
    if (argc < 2)
    {
        std::cout  << "Недостаточно аргументов." << std::endl;
        usageMessage();

        return 0; // Invalid arguments
    }

    else if (argc == 2 && std::string(argv[1]) == "-h")
    {
        usageMessage();
        return 0;
    }

//    std::cout  << "Argc: " << argc << std::endl;

    std::string expr;
    for (int i=1; i<argc; ++i)
    {
        expr += " " + std::string (argv[i]);
    }

//    std::cout  << "Argv expr: " << expr << std::endl;

    bool ok;
    std::vector<std::string> parsed = CcCalculator::parse(expr, &ok);
    if (!ok)
    {
        if (parsed.size() > 0)
        {
            if (parsed.front() == "(" || parsed.front() == ")")
                std::cout << "Некорректный ввод. Неожиданное появление символа в недопустимом месте: ";
            else if (std::isdigit(parsed.front()[0]))
                std::cout << "Некорректный ввод. Неверный формат числа: ";
            else
                std::cout << "Некорректный ввод. Строка содержит недопустимый символ: ";
            std::cout << parsed.front() << std::endl;
        }
        else
            std::cout  << "Некорректный ввод. Cтрока не содержит непустых символов" << std::endl;
        return 0;
    }
    double result = CcCalculator::calculate(parsed, &ok);
    if (!ok)
    {
        std::cout  << "Некорректный ввод. Выражение не может быть вычислено" << std::endl;
        return 0;
    }

    std::stringstream sstream;
    sstream.setf(std::ios::fixed);
    sstream.precision(2);
    sstream << result;

    std::cout << sstream.str() << std::endl;

    return 0;
}
