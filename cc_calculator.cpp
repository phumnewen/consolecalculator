//
// Created by vit on 2/3/21.
//

#include "cc_calculator.h"

bool CcCalculator::isDouble(const std::string & s, double *value)
{
    double val;
    try
    {
        size_t c;
        val = std::stod(s, &c);

        // Если конверсия удалась не от всей строки, то строка целиком - не double
        if (c != s.size())
            return false;
    }
    catch (std::invalid_argument&) {
        return false;
    }
    catch (std::out_of_range&) {
        return false;
    }

    if (value)
        *value = val;
    return true;
}

int CcCalculator::operatorPriority(const char & op)
{
    // Унарные операции помечены как отрицательные
    if (op == -'+' || op == -'-')
        return 4;

    // Левоассоциативные операторы приняты с большим приоритетом
    switch (op)
    {
        case '/':
            return 3;
        case '*':
            return 2;
        case '-':
            return 1;
        case '+':
            return 0;
        default:
            return -1;  // Сюда попадают скобки и всё, что не относится к опраторам
    }
}

bool CcCalculator::calcOperation(std::stack<double> &operands, const char & operator_)
{
    if (operands.empty())
        return false;

    double rhs = operands.top();
    operands.pop();

    // Унарные операции
    if (operator_ < 0)
    {
        switch (-operator_)
        {
            case '+':
                operands.push (rhs);
                break;
            case '-':
                operands.push (-rhs);
                break;
            default:
                operands.push(rhs);
                return false;
        }
        return true;
    }

    if (operands.empty())
    {
        operands.push(rhs);
        return false;
    }

    double lhs = operands.top();
    operands.pop();

    // Бинарные операции
    switch (operator_)
    {
        case '/':
            operands.push(lhs / rhs);
            break;
        case '*':
            operands.push(lhs * rhs);
            break;
        case '-':
            operands.push(lhs - rhs);
            break;
        case '+':
            operands.push(lhs + rhs);
            break;
        default:
            operands.push(rhs);
            operands.push(lhs);
            return false;
    }
    return true;
}

std::vector<std::string> CcCalculator::parse(std::string &expr, bool *success)
{
    *success = false;

    std::vector<std::string> tokens;

    auto end = std::remove_if (expr.begin(), expr.end(), isspace);
    expr.erase(end, expr.end());
    std::replace(expr.begin(), expr.end(), ',','.');

    if (expr.empty())
        return tokens;

    std::string num;
    char prev = ' ';

    for (const char & i : expr)
    {
        if (!isAllowed(i))
        {
            tokens = {{i}};
            return tokens;
        }

        if (isPartOfNumber(i))
        {
            if (prev == ')') // Filter out constructions like "(1+2)3"
            {
                tokens = {{prev}};
                return tokens;
            }
            num += i;
        }

        else {
            if (isDouble(num))
            {
                tokens.push_back(num);
                num = "";
            }
            else if (!num.empty())
            {
                tokens = {{num}};
                return tokens;
            }
            if (isOperator(i) || isBracket(i))
            {
                if (i == ')' && isOperator(prev)) // Filter out constructions like "(1+)3"
                {
                    tokens = {{i}};
                    return tokens;
                }
                if (i == '(' && isPartOfNumber(prev)) // Filter out constructions like "1(2+3)"
                {
                    tokens = {{i}};
                    return tokens;
                }
                tokens.push_back(std::string({i}));
            }
        }
        prev = i;
    }

    if (isDouble(num))
        tokens.push_back(num);

    *success = true;
    return tokens;
}

double CcCalculator::calculate(const std::vector<std::string> &expr, bool *success)
{
    std::stack<char> operators;
    std::stack<double> operands;

    bool operatorIsUnary = true;
    *success = false;

    for (const auto & item: expr)
    {
        if (item == "(")
        {
            operators.push(item.front());
            operatorIsUnary = true; // Следующий оператор может быть унарным
        }
        else if (item == ")")
        {
            operatorIsUnary = false; // Следующий оператор может быть только бинарным
            while (!operators.empty() && operators.top() != '(')
            {
                if (!calcOperation(operands, operators.top()))
                    return .0;
                operators.pop();
            }
            if (operators.empty())
                return .0;
            operators.pop();
        }
        else if (isOperator(item.front()))
        {
            auto op = item.front();
            if (operatorIsUnary && mayBeUnary(op))
                op = -op;
            operatorIsUnary = true; // Следующий оператор может быть унарным
            while (!operators.empty() && operatorPriority(operators.top()) > operatorPriority(op))
            {
                if (!calcOperation(operands, operators.top()))
                    return .0;
                operators.pop();
            }
            operators.push(op);
        }
        else
        {
            operatorIsUnary = false; // Следующий оператор может быть только бинарным
            double val;
            if (!isDouble(item, &val))
                return .0;
            operands.push(val);
        }
    }

    while (!operators.empty())
    {
        if (!calcOperation(operands, operators.top()))
            return .0;
        operators.pop();
    }
    if (operands.size() == 1)
    {
        *success = true;
        return operands.top();
    }
    return .0;
}
